<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::prefix('user')->group(function () {
    Route::post('create', 'UserController@create');
    Route::post('login', 'UserController@login');
    Route::post('forgotten-password', 'UserController@forgottenPassword');
    Route::post('reset-password', 'UserController@resetPassword');

    Route::get('get1', 'UserController@get');

    Route::get('activation', 'UserController@activation');
    
    Route::middleware('auth:api')->get('get', 'UserController@get');
    Route::middleware('auth:api')->post('logout','UserController@logout');
    Route::middleware('auth:api')->post('password-reset','UserController@passwordReset');

});

Route::prefix('subject')->group(function () {

    //  Subject routes
    Route::middleware('auth:api')->post('create', 'SubjectController@create');
    Route::middleware('auth:api')->get('get', 'SubjectController@get');
    Route::middleware('auth:api')->post('get-subject', 'SubjectController@getSubject');
    Route::middleware('auth:api')->post('delete', 'SubjectController@delete');
    Route::middleware('auth:api')->post('update', 'SubjectController@update');

});

Route::prefix('topic')->group(function () {

    //  Topic routes
    Route::middleware('auth:api')->post('create', 'TopicController@create');
    Route::middleware('auth:api')->get('get', 'TopicController@get');
    Route::middleware('auth:api')->post('get-topic', 'TopicController@getTopic');
    Route::middleware('auth:api')->post('update', 'TopicController@update');
    Route::middleware('auth:api')->post('delete', 'TopicController@delete');

});

Route::prefix('flashcard')->group(function () {

    //  Flashcard routes
    Route::middleware('auth:api')->post('create', 'FlashcardController@create');
    Route::middleware('auth:api')->get('get', 'FlashcardController@get');
    Route::middleware('auth:api')->post('get-flashcard', 'FlashcardController@getById');
    Route::middleware('auth:api')->post('update', 'FlashcardController@update');
    Route::middleware('auth:api')->post('delete', 'FlashcardController@delete');
    Route::middleware('auth:api')->post('search', 'FlashcardController@search');
});

