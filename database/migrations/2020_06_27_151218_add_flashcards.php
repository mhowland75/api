<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFlashcards extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subjects', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id');
            $table->string('name', 100);
            $table->dateTime('created_at', 0);
            $table->dateTime('updated_at', 0);
            $table->softDeletes('deleted_at', 0);
        });
        Schema::create('topics', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id');
            $table->foreignId('subject_id');
            $table->string('name', 100);
            $table->dateTime('created_at', 0);
            $table->dateTime('updated_at', 0);
            $table->softDeletes('deleted_at', 0);
        });
        Schema::create('flashcards', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id');
            $table->foreignId('topic_id');
            $table->string('question', 100);
            $table->string('Answer', 100);
            $table->dateTime('created_at', 0);
            $table->dateTime('updated_at', 0);
            $table->softDeletes('deleted_at', 0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subjects');
        Schema::dropIfExists('topics');
        Schema::dropIfExists('flashcards');
    }
}
