<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Subject extends Model
{
    use SoftDeletes;

    protected $with = ['Topic'];
    /**
     * Get the comments for the blog post.
     */
    public function topic()
    {
        return $this->hasMany('App\Topic');
    }
}
