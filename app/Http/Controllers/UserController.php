<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\PasswordReset;
use App\Http\Requests\StoreNewUser;
use App\Http\Requests\Login;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Mail\ActivationEmail;
use App\Mail\ForgottenPassword;
use Carbon\Carbon;

class UserController extends Controller
{
    /**
     * Creates a new user
     */
    public function create(Request $request)
    {
        $errors = $this->validate($request, [
            'name' => 'required|min:2|max:30',
            'email' => 'required|unique:users,email|max:30|min:5|email',
            'password' => 'required|min:6|max:30|string',
        ]);
        if ($errors) {
            return $errors;
        }
        
        $user = new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->activation_token = $this->generateActivationtoken();
        $user->activated = 0;
        $user->save();
        if (!$user) {
            return response()->json([
                'message' => 'failed to create user', 
            ]);
        }

        Mail::to($user->email)->send(new ActivationEmail($user));

        return response()->json([
            'message' => 'success', 
        ]);
    }

    /**
     * Activate the users account
     */
    public function activation (Request $request)
    {
        $activation_token = $request->val;
        $user = User::where('activation_token', $activation_token)->first();

        if ($user) {
            $user->email_verified_at = time();
            $user->activated = 1;
            $user->save();
        }

        return view('activation');
    }

    /**
     * Logs the users in
     */
    public function login(request $request)
    {
        $errors = $this->validate($request, [
            'email' => 'required|max:30|min:5|email',
            'password' => 'required|min:6|max:30|string',
        ]);
        
        $credentials = $request->only('email', 'password');

        $email = $credentials['email'];
        $user = User::where('email', $email)->first();

        if (!$errors) {

            $errors = [];

            if (!$user) {
                $errors['general'] = 'Invalid credentials supplied';  
            } else {
                if (!$user->activated) {
                    $errors['general'] = 'Account requires activation';
                }
            }
    
            if (!Auth::attempt($credentials)) {
                $errors['general'] = 'Invalid credentials supplied';
            }

            if (!empty($error)) {
                return  response()->json([
                    'message' => 'failed',
                    'errors' => $errors
                ]);
            }
        }
        if ($errors) {
            return $errors;
        }

        $token = Auth::user()->createToken('token')->accessToken;

        return response()->json([
            'user' => Auth::user(),
            'token' => $token,
        ]);
    }

    /**
     * Logs out user
     */
    public function logout() {
        if (Auth::check()) {
            Auth::user()->AauthAccessToken()->delete();
            return response()->json([
                'message' => 'Logged out', 
            ]);
        } else {
            return response()->json([
                'error' => 'Logout unsuccessful', 
            ]);
        }
    }

    /**
     * Generates an activation token
     */
    public function generateActivationtoken($length = 10) {
        // TODO: This needs to check if the generated token already exist in the db
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }

        return $randomString;
    }
    /**
     * Handels forgotten password requests
     */
    public function forgottenPassword(Request $request) {

        $errors = $this->validate($request, [
            'email' => 'required|max:30|min:5|email|exists:users',
        ]);
        if ($errors) {
            return $errors;
        }

        $user = User::where('email', $request->email)->first();

        $passwordReset = new PasswordReset;
        $passwordReset->user_id = $user->id;
        $passwordReset->token = $this->generateActivationtoken();
        $passwordReset->save();

        if ($user) {
            Mail::to($user->email)->send(new ForgottenPassword($user));
        }
        
        return response()->json([
            'message' => 'sucsess', 
        ]);
    }
    /**
     * Resets the users password
     */
    public function resetPassword(Request $request) {

        $errors = $this->validate($request, [
            'password' => 'required|string',
            'confirmedPassword' => 'required|string|same:password',
            'token' => 'required|string',
        ]);
        if ($errors) {
            return $errors;
        }

        $pr = PasswordReset::where('token', $request->token)->first();
        $user =  $pr->user;

        $expiryTime = Carbon::parse($pr->created_at)->addMinutes(10);

        if (!$errors) {

            $errors = [];

            if (Carbon::now() > $expiryTime) {
                $errors['general'] = 'Password reset expied. Please request a new password.';
                return  response()->json([
                    'message' => 'failed',
                    'errors' => $errors
                ]);
            }

        }

        $user->password = bcrypt($request->password);
        $user->save();

        return response()->json([
            'message' => 'sucsess', 
        ]);

    }
}
