<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Flashcard;
use Illuminate\Support\Facades\Auth;

class FlashcardController extends Controller
{
    /**
     * creates a new flashcard
     */
    public function create(Request $request)
    {
        $errors = $this->validate($request, [
            'question' => 'required|min:2|max:30',
            'answer' => 'required|min:1|max:30',
            'topic_id' => 'required|numeric|exists:topics,id',
        ]);

        if ($errors) {
            return $errors;
        }

        $flashcard = new Flashcard;
        $flashcard->user_id = Auth::user()->id;
        $flashcard->question = $request->question;
        $flashcard->answer = $request->answer;
        $flashcard->topic_id = $request->topic_id;
        $flashcard->save();

        if (empty($flashcard)) {
            return response()->json([
                'message' => 'failed',
                'error' => 'flashcard not created'
            ]);
        }

        return response()->json([
            'message' => 'Flashcard created', 
        ]);
    }

    /**
     *   Returns the users flashcards
     */
    public function get(Request $request)
    {
        $data = $request->validate([
            'topic_id' => 'required|numeric',
        ]);
        if (isset($data->error)) {
            return $data;
        }

        $flashcards = Flashcard::where('user_id', Auth::user()->id)
        ->where('topic_id', $data['topic_id'])
        ->get();

        if (empty($flashcards->first())) {
            return response()->json([
                'message' => 'failed',
                'error' => 'No flashcards found'
            ]);
        }

        return response()->json([
            'message' => 'succsess', 
            'flashcards' => $flashcards,
        ]);
    }

    /**
     *   Returns a flashcard by
     */
    public function getById(Request $request)
    {
        $errors = $this->validate($request, [
            'flashcard_id' => 'required|numeric|exists:flashcards,id',
        ]);

        if ($errors) {
            return $errors;
        }

        $flashcard = Flashcard::where('user_id', Auth::user()->id)
        ->where('id', $request->flashcard_id)
        ->first();

        if (empty($flashcard)) {
            return response()->json([
                'message' => 'failed',
                'error' => 'No flashcard found'
            ]);
        }

        return response()->json([
            'message' => 'succsess', 
            'flashcard' => $flashcard,
        ]);
    }
    
    /**
     *   Updates a flashcard
     */
    public function update(Request $request)
    {
        $errors = $this->validate($request, [
            'question' => 'required|min:2|max:30',
            'answer' => 'required|min:1|max:30',
            'flashcard_id' => 'required|numeric|exists:flashcards,id',
        ]);

        if ($errors) {
            return $errors;
        }
        
        $flashcard = Flashcard::where('user_id', Auth::user()->id)
        ->where('id', $request->flashcard_id)
        ->first();

        if (empty($flashcard)) {
            return response()->json([
                'message' => 'failed',
                'error' => 'No flashcard found'
            ]);
        }
        
        $flashcard->question = $request->question;
        $flashcard->answer = $request->answer;
        $flashcard->save();

        return response()->json([
            'message' => 'succsess', 
        ]);
    }

    /**
     * Deletes a flashcard
     */
    public function delete(Request $request)
    {
        $errors = $this->validate($request, [
            'flashcard_id' => 'required|numeric|exists:flashcards,id',
        ]);

        if ($errors) {
            return $errors;
        }
        
        $flashcard = Flashcard::where('user_id', Auth::user()->id)
        ->where('id', $request->flashcard_id)
        ->first();

        if (empty($flashcard)) {
            return response()->json([
                'message' => 'failed',
                'error' => 'No flashcard found'
            ]);
        }

        $flashcard->delete();

        return response()->json([
            'message' => 'sucsess', 
        ]);
    }

    /**
     *   Returns the users searched flashcards
     */
    public function search(Request $request)
    {
        $flashcards = Flashcard::where('user_id', Auth::user()->id)
        ->where('topic_id', $request->topic_id)
        ->where('question', 'like', $request->search . '%')
        ->get();

        return response()->json([
            'message' => 'succsess', 
            'flashcards' => $flashcards,
        ]);
    }
}
