<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Subject;
use Illuminate\Support\Facades\Auth;

class SubjectController extends Controller
{
    /**
     * Creates a new subject
     */
    public function create(Request $request)
    {
        $errors = $this->validate($request, [
            'name' => 'required|min:2|max:30',
        ]);

        if ($errors) {
            return $errors;
        }

        $subject = new Subject;
        $subject->name = $request->name;
        $subject->user_id = Auth::user()->id;
        $subject->save();

        if (!$errors) {

            $errors['general'] = 'Subject not found';

            if (!empty($error)) {
                return  response()->json([
                    'message' => 'failed',
                    'errors' => $errors
                ]);
            }
        }

        return response()->json([
            'message' => 'sucsses', 
        ]);
    }

    /**
     *   Returns the users subjects
     */
    public function get()
    {
        $subjects = Subject::where('user_id', Auth::user()->id)->get();

        if(!$subjects->first()) {
            return response()->json([
                'message' => 'failed',
                'error' => 'Unable to find subjects'
            ]);
        }

        return response()->json([
            'message' => 'success',
            'subjects' => $subjects,
        ]);
    }

    /**
     *   Returns the spesfic subject from id
     */
    public function getSubject(Request $request)
    {
        $errors = $this->validate($request, [
            'subject_id' => 'required|numeric|exists:subjects,id',
        ]);

        if ($errors) {
            return $errors;
        }

        $subject = Subject::where('user_id', Auth::user()->id)
        ->where('id', $request->subject_id)
        ->first();

        if(!$subject) {
            return response()->json([
                'message' => 'failed',
                'error' => 'Unable to find subject'
            ]);
        }
        
        return response()->json([
            'message' => 'success', 
            'subject' => $subject,
        ]);
    }

    /**
     * Updates a subject recored; 
     */
    public function update(Request $request)
    {
        $errors = $this->validate($request, [
            'name' => 'required|min:2|max:30',
            'subject_id' => 'required|numeric|exists:subjects,id',
        ]);

        if ($errors) {
            return $errors;
        }

        $subject = Subject::where('user_id', Auth::user()->id)
        ->where('id', $request->subject_id)
        ->first();

        $subject->name = $request->name;
        $subject->save();


        return response()->json([
            'message' => 'sucsses', 
        ]);
    }

    /**
     * Deletes a subject
     */
    public function delete(Request $request)
    {
        $errors = $this->validate($request, [
            'subject_id' => 'required|numeric|exists:subjects,id',
        ]);

        if ($errors) {
            return $errors;
        }

        $subject = Subject::with('topic.flashcards')
        ->where('user_id', Auth::user()->id)
        ->where('id', $request->subject_id)
        ->first();

        if (!$subject) {

            $errors['general'] = 'Subject not found';

            return  response()->json([
                'message' => 'failed',
                'errors' => $errors
            ]);
        }
    

        if ($subject->topic != []) {

            foreach($subject->topic as $topic) {
                if ($topic->flashcards != []) {

                    foreach($topic->flashcards as $flashcard) {
                        $flashcard->delete();
                    }
                }
                $topic->delete();
            }
        }

        $subject->delete();

        return response()->json([
            'message' => 'sucssess', 
        ]);
    }

}
