<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Validator;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function validate($request, $rules) {
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return  response()->json([
                'message' => 'failed',
                'errors' => $validator->messages()
            ]);
        }

        return false;
    }
}
