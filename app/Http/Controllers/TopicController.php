<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Topic;
use Illuminate\Support\Facades\Auth;

class TopicController extends Controller
{
    /**
     * Creates a new topic
     */
    public function create(Request $request)
    {
        $errors = $this->validate($request, [
            'name' => 'required|min:2|max:30',
            'subject_id' => 'required|numeric|exists:subjects,id',
        ]);

        if ($errors) {
            return $errors;
        }

        $topic = new Topic;
        $topic->name = $request->name;
        $topic->user_id = Auth::user()->id;
        $topic->subject_id = $request->subject_id;
        $topic->save();
        if (!$topic) {
            return response()->json([
                'message' => 'failed',
                'error' => 'Topic not created' 
            ]);
        }

        return response()->json([
            'message' => 'sucsess', 
        ]);
    }

    /**
     *   Returns the users topics
     */
    public function get(Request $request)
    {
        $errors = $this->validate($request, [
            'subject_id' => 'required|numeric|exists:subjects,id',
        ]);

        if ($errors) {
            return $errors;
        }

        $topics = Topic::with('flashcards')
        ->where('user_id', Auth::user()->id)
        ->where('subject_id', $request->subject_id)
        ->get();

        if (empty($topics->first())) {
            return response()->json([
                'message' => 'failed', 
                'error' => 'No topics found'
            ]);
        }

        return response()->json([
            'message' => 'succsess', 
            'topics' => $topics,
        ]);
    }

    /**
     *   Returns the spesfic topic from id
     */
    public function getTopic(Request $request)
    {
        $errors = $this->validate($request, [
            'topic_id' => 'required|numeric|exists:topics,id',
        ]);

        if ($errors) {
            return $errors;
        }

        $topic = Topic::where('user_id', Auth::user()->id)
        ->where('id', $request->topic_id)
        ->first();

        if (!$topic) {
            return response()->json([
                'message' => 'failed',  
                'error' => 'Unable to find topic',
            ]);
        }

        return response()->json([
            'message' => 'success',  
            'topic' => $topic,
        ]);
    }

    /**
     * Updates a topic recored; 
     */
    public function update(Request $request)
    {
        $errors = $this->validate($request, [
            'name' => 'required|min:2|max:30',
            'topic_id' => 'required|numeric|exists:topics,id',
        ]);

        if ($errors) {
            return $errors;
        }

        $topic = Topic::where('id', $request->topic_id)
        ->where('user_id', Auth::user()->id)
        ->first();

        if (empty($topic)) {
            return response()->json([
                'message' => 'failed',  
                'error' => 'Unable to find topic',
            ]);
        }

        $topic->name = $request->name;
        $topic->save();

        return response()->json([
            'message' => 'successful', 
        ]);
    }

    /**
     * Deletes a topic and all the related flashcards
     */
    public function delete(Request $request)
    {
        $errors = $this->validate($request, [
            'topic_id' => 'required|numeric|exists:topics,id',
        ]);

        if ($errors) {
            return $errors;
        }

        $topic = Topic::with('flashcards')
        ->where('id', $request->topic_id)
        ->where('user_id', Auth::user()->id)
        ->first();

        if (empty($topic)) {
            return response()->json([
                'message' => 'failed',
                'error' => 'Unable to find topic'
            ]);
        }

        foreach($topic->flashcards as $flashcard) {
            $flashcard->delete();
        }
        $topic->delete();

        return response()->json([
            'message' => 'Topic deleted', 
        ]);
    }
}
