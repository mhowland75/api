<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PasswordReset extends Model
{
        /**
     * Define the relationship to the password resets table
     */
    public function user(){
        
        return $this->belongsTo('\App\User');
    }
}
