<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Topic extends Model
{
    use SoftDeletes;

    /**
     * Defines the relationships to flashcards.
     */
    public function flashcards()
    {
        return $this->hasMany('App\Flashcard');
    }
}
